#!/bin/bash

source /herwig/bin/activate
export PATH="${PATH}:/herwig/bin/"
export PYTHONPATH="${PYTHONPATH}:/herwig/bin/"
export PYTHONPATH="${PYTHONPATH}:/herwig/lib/"
export PATH="$(find /herwig/opt -maxdepth 2 -name 'bin'):$PATH"
export RIVET_ANALYSIS_PATH="$PWD:$RIVET_ANALYSIS_PATH"

if  test "$1" = "lhapdf" && test "$2" = "install" && ! test -d "$PWD/.lhapdf"; then
	echo -e "Migrating LHAPDF PDF sets ..."
	mkdir .lhapdf && cp -r /herwig/share/LHAPDF/* $PWD/.lhapdf && \
	echo -e "PDF sets have been migrated to: $PWD/.lhapdf"
fi

if [ -d "$PWD/.lhapdf" ]; then
	export LHAPATH="$PWD/.lhapdf"
fi

exec "$@"
