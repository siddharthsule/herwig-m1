# Herwig on Apple Silicon

This is work in progress to get Herwig working on Apple Silicon Devices. The way we do it is to build a docker image of Ubuntu 22.08 arm64/v8 on an M1 MacBook by using the Herwig Bootstrap.

You can view the current docker image at: <https://hub.docker.com/r/siddharthsule/herwig-m1>

## Pulling and Running

To pull the image, use the command:

```bash
docker pull siddharthsule/herwig-m1
```

To run the image, the best command is:

```bash
alias DRH='docker run -i --rm -u `id -u $USER`:`id -g` -v $PWD:$PWD -w $PWD siddharthsule/herwig-m1'
DRH <command> # Herwig, Rivet and More
```

## Deviations from the original Bootstrap

- OpenLoops: now build with cmodel=small
- Herwig: build with -fno-range-check and -fdefault-integer-8

## Current Issues

Herwig + Madgraph DOES NOT WORK! The two work independantly, but there seems to be issues calling madgraph from Herwig.

- Update 1: Edited InterfaceMadGraph.f to avoil c binding issues (to use, run once, replace with original file in Herwig-cache and make)

There is an in file for testing purposes!
