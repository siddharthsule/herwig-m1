!This file gets filled from mg2herwig

! In this version, we change some of the code to have explicit c bindings
! This prevents one of the errors, but phase space generation is not fully
! functional yet.
!///////////////////////////////////////////////////////////////////////


       subroutine MGInitProc(file_card)
         IMPLICIT NONE
         character*(*) file_card
C         print*,"Reading inputs from: "
C         print*, TRIM(file_card)
         CALL setpara(file_card)
       END

!///////////////////////////////////////////////////////////////////////

      subroutine MG_Calculate_wavefunctions_virt(proc, momenta, virt) bind(c, name="MG_Calculate_wavefunctions_virt")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT), value :: proc
         REAL(C_DOUBLE), intent(in) :: momenta(1:41), virt(1:20)
      END


!///////////////////////////////////////////////////////////////////////

      subroutine MG_Calculate_wavefunctions_born(proc, momenta, hel) bind(c, name="MG_Calculate_wavefunctions_born")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT), value :: proc
         REAL(C_DOUBLE), intent(in) :: momenta(1:41)
         INTEGER(C_INT), intent(in) :: hel(1:11)

         SELECT CASE (proc)
         CASE(1)
            CALL MG5_1_BORN(momenta,hel)
         CASE(2)
            CALL MG5_2_BORN(momenta,hel)
         CASE(3)
            CALL MG5_3_BORN(momenta,hel)
         CASE(4)
            CALL MG5_4_BORN(momenta,hel)
         CASE(5)
            CALL MG5_5_BORN(momenta,hel)
         CASE(6)
            CALL MG5_6_BORN(momenta,hel)
         CASE(7)
            CALL MG5_7_BORN(momenta,hel)
         CASE(8)
            CALL MG5_8_BORN(momenta,hel)
         CASE(9)
            CALL MG5_9_BORN(momenta,hel)
         CASE(10)
            CALL MG5_10_BORN(momenta,hel)
         CASE DEFAULT
            WRITE(*,*) '##W02A WARNING No id found '
         END SELECT    
      END subroutine MG_Calculate_wavefunctions_born

!///////////////////////////////////////////////////////////////////////

      subroutine MG_Jamp(proc ,color,amp) bind(c, name="MG_Jamp")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT), value :: proc, color
         REAL(C_DOUBLE) :: amp(1:2)
         COMPLEX(C_DOUBLE_COMPLEX) :: Jamp
         SELECT CASE (proc)
         CASE(1)
            CALL MG5_1_GET_JAMP(color,Jamp)
         CASE(2)
            CALL MG5_2_GET_JAMP(color,Jamp)
         CASE(3)
            CALL MG5_3_GET_JAMP(color,Jamp)
         CASE(4)
            CALL MG5_4_GET_JAMP(color,Jamp)
         CASE(5)
            CALL MG5_5_GET_JAMP(color,Jamp)
         CASE(6)
            CALL MG5_6_GET_JAMP(color,Jamp)
         CASE(7)
            CALL MG5_7_GET_JAMP(color,Jamp)
         CASE(8)
            CALL MG5_8_GET_JAMP(color,Jamp)
         CASE(9)
            CALL MG5_9_GET_JAMP(color,Jamp)
         CASE(10)
            CALL MG5_10_GET_JAMP(color,Jamp)
         CASE DEFAULT
            WRITE(*,*) '##W02A WARNING No id found '
         END SELECT    

         amp(1)=real(Jamp)
         amp(2)=aimag(Jamp)   
      END
       
!///////////////////////////////////////////////////////////////////////

      subroutine MG_LNJamp(proc ,color,amp) bind(c, name="MG_LNJamp")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT) ::    proc,color
         REAL(C_DOUBLE) :: amp(2)
         COMPLEX(C_DOUBLE_COMPLEX) :: Jamp
         SELECT CASE (proc)
         CASE(1)
            CALL MG5_1_GET_LNJAMP(color,Jamp)
         CASE(2)
            CALL MG5_2_GET_LNJAMP(color,Jamp)
         CASE(3)
            CALL MG5_3_GET_LNJAMP(color,Jamp)
         CASE(4)
            CALL MG5_4_GET_LNJAMP(color,Jamp)
         CASE(5)
            CALL MG5_5_GET_LNJAMP(color,Jamp)
         CASE(6)
            CALL MG5_6_GET_LNJAMP(color,Jamp)
         CASE(7)
            CALL MG5_7_GET_LNJAMP(color,Jamp)
         CASE(8)
            CALL MG5_8_GET_LNJAMP(color,Jamp)
         CASE(9)
            CALL MG5_9_GET_LNJAMP(color,Jamp)
         CASE(10)
            CALL MG5_10_GET_LNJAMP(color,Jamp)
         CASE DEFAULT
            WRITE(*,*) '##W02A WARNING No id found '
         END SELECT    

         amp(1)=real(Jamp)
         amp(2)=aimag(Jamp)   
      END



!///////////////////////////////////////////////////////////////////////
! DIFFERENT CODE HERE
!///////////////////////////////////////////////////////////////////////

       subroutine MG_NCol(proc ,color)
     $   bind(c, name="MG_NCol")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT), intent(in) :: proc
         INTEGER(C_INT), intent(out) :: color
         SELECT CASE (proc)
         CASE(1)
            CALL MG5_1_GET_NCOL(color)
         CASE(2)
            CALL MG5_2_GET_NCOL(color)
         CASE(3)
            CALL MG5_3_GET_NCOL(color)
         CASE(4)
            CALL MG5_4_GET_NCOL(color)
         CASE(5)
            CALL MG5_5_GET_NCOL(color)
         CASE(6)
            CALL MG5_6_GET_NCOL(color)
         CASE(7)
            CALL MG5_7_GET_NCOL(color)
         CASE(8)
            CALL MG5_8_GET_NCOL(color)
         CASE(9)
            CALL MG5_9_GET_NCOL(color)
         CASE(10)
            CALL MG5_10_GET_NCOL(color)
         CASE DEFAULT
             WRITE(*,*) '##W02A WARNING No id found '
         END SELECT    

       END

!///////////////////////////////////////////////////////////////////////

       subroutine MG_Colour(proc,i,j ,color)
     $   bind(c, name="MG_Colour")
         use, intrinsic :: iso_c_binding
         IMPLICIT NONE
         INTEGER(C_INT), intent(in) :: proc
         INTEGER(C_INT), intent(in) :: i
         INTEGER(C_INT), intent(in) :: j
         INTEGER(C_INT), intent(out) :: color
         SELECT CASE (proc)
         CASE(1)
            CALL MG5_1_GET_NCOLOR(i,j,color)
         CASE(2)
            CALL MG5_2_GET_NCOLOR(i,j,color)
         CASE(3)
            CALL MG5_3_GET_NCOLOR(i,j,color)
         CASE(4)
            CALL MG5_4_GET_NCOLOR(i,j,color)
         CASE(5)
            CALL MG5_5_GET_NCOLOR(i,j,color)
         CASE(6)
            CALL MG5_6_GET_NCOLOR(i,j,color)
         CASE(7)
            CALL MG5_7_GET_NCOLOR(i,j,color)
         CASE(8)
            CALL MG5_8_GET_NCOLOR(i,j,color)
         CASE(9)
            CALL MG5_9_GET_NCOLOR(i,j,color)
         CASE(10)
            CALL MG5_10_GET_NCOLOR(i,j,color)
         CASE DEFAULT
             WRITE(*,*) '##W02A WARNING No id found '
         END SELECT    

       END

!///////////////////////////////////////////////////////////////////////
       
      subroutine MG_vxxxxx(p, n, inc, VC) bind(c, name='MG_vxxxxx')
         use iso_c_binding, only : c_double, c_double_complex, c_int
         implicit none
         real (c_double), dimension(4), intent(in) :: p
         real (c_double), dimension(4), intent(in) :: n
         integer(c_int), value :: inc
         real (c_double), dimension(8), intent(out) :: VC
         complex (c_double_complex), dimension(7) :: VCtmp
         call vxxxxx(p, 0d0, 1, inc, VCtmp)
         VC(1)= real(VCtmp(4))
         VC(2)=aimag(VCtmp(4))
         VC(3)= real(VCtmp(5))
         VC(4)=aimag(VCtmp(5))
         VC(5)= real(VCtmp(6))
         VC(6)=aimag(VCtmp(6))
         VC(7)= real(VCtmp(7))
         VC(8)=aimag(VCtmp(7))
      end subroutine MG_vxxxxx
    
